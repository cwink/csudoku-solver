PROJECT := csudoku-solver

CC := clang

CFLAGS := $(CFLAGS) -std=c89 -Weverything -pedantic-errors -O0 -g
LDFLAGS := $(LDFLAGS)

all : $(PROJECT).x

$(PROJECT).x : $(PROJECT).o
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(PROJECT).o : $(PROJECT).c
	$(CC) $(CFLAGS) -c -o $@ $<

clean :
	rm -rf *.o
	rm -rf $(PROJECT).x

run :
	./$(PROJECT).x

valgrind :
	valgrind --suppressions=.valgrind --leak-check=full ./$(PROJECT).x

format :
	clang-format -i -style=gnu *.h
	clang-format -i -style=gnu *.c

