#include <stdio.h>
#include <stdlib.h>

#define PUZZLE_WIDTH 9
#define PUZZLE_HEIGHT 9
#define PUZZLE_DIVISOR 3
#define PUZZLE_TRUE 1
#define PUZZLE_FALSE 0
#define PUZZLE_MAX_NUM '9'

static char current_puzzle[PUZZLE_HEIGHT][PUZZLE_WIDTH]
    = { { '5', '3', ' ', ' ', '7', ' ', ' ', ' ', ' ' },
        { '6', ' ', ' ', '1', '9', '5', ' ', ' ', ' ' },
        { ' ', '9', '8', ' ', ' ', ' ', ' ', '6', ' ' },
        { '8', ' ', ' ', ' ', '6', ' ', ' ', ' ', '3' },
        { '4', ' ', ' ', '8', ' ', '3', ' ', ' ', '1' },
        { '7', ' ', ' ', ' ', '2', ' ', ' ', ' ', '6' },
        { ' ', '6', ' ', ' ', ' ', ' ', '2', '8', ' ' },
        { ' ', ' ', ' ', '4', '1', '9', ' ', ' ', '5' },
        { ' ', ' ', ' ', ' ', '8', ' ', ' ', '7', '9' } };

unsigned char puzzle_valid_row (const char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH],
                                const unsigned char y, const char n);

unsigned char
puzzle_valid_column (const char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH],
                     const unsigned char x, const char n);

unsigned char puzzle_valid_box (const char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH],
                                const unsigned char x, const unsigned char y,
                                const char n);

unsigned char puzzle_valid (const char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH],
                            const unsigned char x, const unsigned char y,
                            const char n);

void puzzle_print (const char puzzle[PUZZLE_HEIGHT][PUZZLE_WIDTH]);

unsigned char puzzle_solver (char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH]);

unsigned char puzzle_unassigned (char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH],
                                 unsigned char *x, unsigned char *y);

int
main (void)
{
  puzzle_print (current_puzzle);

  puzzle_solver (current_puzzle);

  fprintf (stdout, "\n");

  puzzle_print (current_puzzle);

  return EXIT_SUCCESS;
}

unsigned char
puzzle_valid_row (const char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH],
                  const unsigned char y, const char n)
{
  unsigned char index = 0;

  for (index = 0; index < PUZZLE_WIDTH; ++index)
    {
      if (pieces[y][index] == n)
        {
          return PUZZLE_FALSE;
        }
    }

  return PUZZLE_TRUE;
}

unsigned char
puzzle_valid_column (const char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH],
                     const unsigned char x, const char n)
{
  unsigned char index = 0;

  for (index = 0; index < PUZZLE_HEIGHT; ++index)
    {
      if (pieces[index][x] == n)
        {
          return PUZZLE_FALSE;
        }
    }

  return PUZZLE_TRUE;
}

unsigned char
puzzle_valid_box (const char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH],
                  const unsigned char x, const unsigned char y, const char n)
{
  unsigned char vx = 0;
  unsigned char vy = 0;
  unsigned char mx = PUZZLE_DIVISOR;
  unsigned char my = PUZZLE_DIVISOR;

  if (x >= PUZZLE_DIVISOR && x < PUZZLE_DIVISOR * 2)
    {
      vx = PUZZLE_DIVISOR;
      mx = PUZZLE_DIVISOR * 2;
    }
  else if (x >= PUZZLE_DIVISOR * 2 && x < PUZZLE_DIVISOR * 3)
    {
      vx = PUZZLE_DIVISOR * 2;
      mx = PUZZLE_DIVISOR * 3;
    }

  if (y >= PUZZLE_DIVISOR && y < PUZZLE_DIVISOR * 2)
    {
      vy = PUZZLE_DIVISOR;
      my = PUZZLE_DIVISOR * 2;
    }
  else if (y >= PUZZLE_DIVISOR * 2 && y < PUZZLE_DIVISOR * 3)
    {
      vy = PUZZLE_DIVISOR * 2;
      my = PUZZLE_DIVISOR * 3;
    }

  for (; vy < my; ++vy)
    {
      for (; vx < mx; ++vx)
        {
          if (pieces[vy][vx] == n)
            {
              return PUZZLE_FALSE;
            }
        }
    }

  return PUZZLE_TRUE;
}

unsigned char
puzzle_valid (const char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH],
              const unsigned char x, const unsigned char y, const char n)
{
  return puzzle_valid_row (pieces, y, n) && puzzle_valid_column (pieces, x, n)
                 && puzzle_valid_box (pieces, x, y, n)
             ? PUZZLE_TRUE
             : PUZZLE_FALSE;
}

void
puzzle_print (const char puzzle[PUZZLE_HEIGHT][PUZZLE_WIDTH])
{
  unsigned char x = 0;
  unsigned char y = 0;
  unsigned char index = 0;

  for (y = 0; y < PUZZLE_HEIGHT; ++y)
    {
      for (x = 0; x < PUZZLE_WIDTH; ++x)
        {
          fprintf (stdout, "%c", puzzle[y][x]);

          if ((x + 1) % PUZZLE_DIVISOR == 0 && (x + 1) != PUZZLE_WIDTH)
            {
              fprintf (stdout, "|");
            }
        }

      fprintf (stdout, "\n");

      if ((y + 1) % PUZZLE_DIVISOR == 0 && (y + 1) != PUZZLE_HEIGHT)
        {
          for (index = 0; index < PUZZLE_WIDTH + PUZZLE_DIVISOR - 1; ++index)
            {
              fprintf (stdout, "-");
            }

          fprintf (stdout, "\n");
        }
    }
}

unsigned char
puzzle_solver (char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH])
{
  unsigned char x = 0;
  unsigned char y = 0;
  char n = 0;

  if (puzzle_unassigned (pieces, &x, &y) == PUZZLE_TRUE)
    {
      return PUZZLE_TRUE;
    }

  for (n = '1'; n <= PUZZLE_MAX_NUM; ++n)
    {
      if (puzzle_valid (pieces, x, y, n))
        {
          pieces[y][x] = n;

          if (puzzle_solver (pieces))
            {
              return PUZZLE_TRUE;
            }

          pieces[y][x] = ' ';
        }
    }

  return PUZZLE_FALSE;
}

unsigned char
puzzle_unassigned (char pieces[PUZZLE_HEIGHT][PUZZLE_WIDTH], unsigned char *x,
                   unsigned char *y)
{
  unsigned char i = 0;
  unsigned char j = 0;

  for (j = 0; j < PUZZLE_HEIGHT; ++j)
    {
      for (i = 0; i < PUZZLE_WIDTH; ++i)
        {
          if (pieces[j][i] == ' ')
            {
              *x = i;
              *y = j;

              return PUZZLE_FALSE;
            }
        }
    }

  return PUZZLE_TRUE;
}
